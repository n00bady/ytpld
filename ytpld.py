from pytube import Playlist
import sys
import os

def main():
    # Check if the first argument is a valid youtube playlist url
    # Should I move this in the if __name__ = '__main__' together with the exception or keep it here ???
    if sys.argv[1] not in "https://www.youtube.com/playlist?list=":
        print("This is not a youtube playlist url!")
        sys.exit()
    # Print the url(hopefully in futere the playlist name) of the playlist and notify the user that the download has started
    # hopefully in the near future I'll be able to show the downloading proccess too
    pl = Playlist(sys.argv[1])
    print("Downloading playlist: "+str(sys.argv[1]))
    pl.download_all()
    # Notify the user that the download finished and exit
    print("Download finished!")
    sys.exit()

if __name__ == '__main__':
    try:
        arg1 = sys.argv[1]
    except IndexError:
        print("Usage: " + os.path.basename(__file__) + " <arg1>")
        sys.exit(1)

    main()
